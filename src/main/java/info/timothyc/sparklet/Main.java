package info.timothyc.sparklet;

import akka.actor.ActorSystem;
import com.google.common.collect.Lists;
import com.sun.rowset.internal.Row;
import info.timothyc.sparklet.akka.AkkaHandleFactory;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static info.timothyc.sparklet.expr.Row.*;

public class Main {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        ActorSystem system = ActorSystem.create();
        try {
//            system.logConfiguration();

            HandleFactory hf = new AkkaHandleFactory(system);

            RDD left = RDD.from(hf, l(
                    l(1, 2, "abc"),
                    l(5, 10, "yo"),
                    l(9, -1, "neg"),
                    l(1, -1, "#####!")
            ));

            RDD re = left
                .repartition(3, 1)
//                .select(0, 2)
                .get(5, TimeUnit.SECONDS)
                .map(row -> row.add(plus(c(0), mul(c(1), lit(2)))).drop(0))
                .get();
            Future<Iterable<?>> selection = re.retrieve();

            for (Object s : selection.get()) {
                System.out.println(" * " + s);
            }
            System.out.println("count = " + left.count().get() + " = " + re.count().get());
        } finally {
            system.terminate();
        }
    }

    static <E> List<E> l(E... elements) {
        return Lists.newArrayList(elements);
    }
}
