package info.timothyc.sparklet.akka;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import com.google.common.collect.Lists;
import info.timothyc.sparklet.Handle;
import info.timothyc.sparklet.expr.Row;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class AkkaWorker extends AbstractActor {

    private final List<List<?>> data;

    public AkkaWorker() {
        this.data = Lists.newArrayList();
    }

    public AkkaWorker(List<List<?>> data) {
        this.data = Lists.newArrayList(data);
    }

    static int hash(Object value) {
        return value != null ? value.hashCode() : 0; // TODO
    }

    static int pmod(int num, int mod) {
        int pm = num % mod;
        return (pm < 0) ? pm + mod : pm;
    }

    private void repartition(ActorRef sender, Messages.Repartition cmd) {
//        System.out.println("repartition worker index " + cmd.columnIndex + " across " + cmd.handles.size() + " from " + data);
        int numPartitions = cmd.handles.size();
        CompletableFuture<Void>[] tasks = new CompletableFuture[data.size()];
        int t = 0;
        for (List<?> row : data) {
            int newPartition = pmod(hash(row.get(cmd.columnIndex)), numPartitions);
//                    System.out.println("SEND " + row + " to " + (hash(row.get(cmd.columnIndex))) + " % " + numPartitions + " = " + newPartition);
            tasks[t++] = cmd.handles.get(newPartition).append(row);
        }
        CompletableFuture.allOf(tasks)
            .thenRun(() -> sender.tell(new Messages.Response(cmd.id, null), getSelf()));
    }

    public Receive createReceive() {
        return receiveBuilder()
            .match(Messages.Set.class, set -> {
                System.out.println("worker got " + set);
                data.clear();
                data.addAll(set.data);
            })
            .match(Messages.Append.class, cmd -> {
                data.add(cmd.row);
                sender().tell(new Messages.Response(cmd.id, null), getSelf());
            })
            .match(Messages.Repartition.class, cmd -> {
                repartition(getSender(), cmd);
            })
            .match(Messages.Map.class, cmd -> map(getSender(), cmd))
            .match(Messages.Count.class, cmd -> {
                System.out.println("worker count " + data.size());
                sender().tell(new Messages.Response(cmd.id, data.size()), getSelf());
            })
            .match(Messages.Retrieve.class, cmd -> {
                System.out.println("worker retrieve " + data);
                getSender().tell(new Messages.Response(cmd.id, data), getSelf());
            })
            .build();
    }

    private void map(ActorRef sender, Messages.Map cmd) {
        CompletableFuture<Void>[] steps = new CompletableFuture[data.size()];
        for (int i = 0; i < data.size(); ++i) {
            List<?> row = data.get(i);
            Row newRow = cmd.transform.apply(new Row(row));
            System.out.println("MAP " + row + " --> " + newRow.data);
            steps[i] = cmd.destination.append(newRow.data);
        }
        CompletableFuture.allOf(steps).thenRun(() ->
            sender.tell(new Messages.Response(cmd.id, null), getSelf()));
    }
}
