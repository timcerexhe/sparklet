package info.timothyc.sparklet.akka;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import info.timothyc.sparklet.Handle;
import info.timothyc.sparklet.Partition;
import info.timothyc.sparklet.expr.Row;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

public class AkkaHandle implements Handle {

    private final AtomicInteger task = new AtomicInteger();
    private final Map<Integer, CompletableFuture<?>> responses = new ConcurrentHashMap<>();
    private final ActorRef master;
    private final ActorRef ref;

    private static class Self extends AbstractActor {

        private final Map<Integer, CompletableFuture<?>> responses;

        private Self(Map<Integer, CompletableFuture<?>> responses) {
            this.responses = responses;
        }

        @Override
        public Receive createReceive() {
            return new ReceiveBuilder()
                .match(Messages.Response.class, r -> {
//                    System.out.println("GOT RESPONSE " + r.id);
//                    System.out.println("RESPONSES " + responses);
                    CompletableFuture<?> f = responses.get(r.id);
                    ((CompletableFuture<Object>) f).complete(r.data);
                })
                .build();
        }
    }

    public AkkaHandle(ActorSystem system, ActorRef ref) {
        Props props = Props.create(Self.class, responses);
        this.master = system.actorOf(props);
        this.ref = ref;
    }

//    public static <L extends Iterable<E>, E extends Iterable<?>> AkkaHandle from(L things) {
//        return new AkkaHandle();
//    }

    private int id() {
        return task.incrementAndGet();
    }

    private <T> CompletableFuture<T> sendRecv(Function<Integer, Object> messager) {
        int id = id();
        CompletableFuture<Integer> future = new CompletableFuture<>();
        responses.put(id, future);
        ref.tell(messager.apply(id), master);
        CompletableFuture<T> value = (CompletableFuture<T>) responses.get(id);
//        responses.remove(id);
        return value;
    }

    @Override
    public CompletableFuture<Void> append(List<?> row) {
        return sendRecv(id -> new Messages.Append(id, row));
    }

    @Override
    public CompletableFuture<Void> repartition(int columnIndex, List<Handle> newPartitions) {
        return sendRecv(id -> new Messages.Repartition(id, columnIndex, newPartitions));
    }

    @Override
    public CompletableFuture<Void> map(Function<Row, Row> transform, Handle newPartition) {
        return sendRecv(id -> new Messages.Map(id, transform, newPartition));
    }

    @Override
    public CompletableFuture<Integer> count() {
        return sendRecv(Messages.Count::new);
    }

    @Override
    public CompletableFuture<Iterable<?>> retrieve() {
        return sendRecv(Messages.Retrieve::new);
    }

}
