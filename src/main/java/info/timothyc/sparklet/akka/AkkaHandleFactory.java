package info.timothyc.sparklet.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import info.timothyc.sparklet.Handle;
import info.timothyc.sparklet.HandleFactory;

import java.util.Collections;

public class AkkaHandleFactory implements HandleFactory {

    private final ActorSystem system;

    public AkkaHandleFactory(ActorSystem system) {
        this.system = system;
    }

    public <L extends Iterable<E>, E extends Iterable<?>> Handle empty() {
        return from(Collections.emptyList());
    }

    public <L extends Iterable<E>, E extends Iterable<?>> Handle from(L elements) {
        Props props = Props.create(AkkaWorker.class, elements);
        ActorRef ref = system.actorOf(props);
        return new AkkaHandle(system, ref);
    }
}
