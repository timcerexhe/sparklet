package info.timothyc.sparklet.akka;

import info.timothyc.sparklet.Handle;
import info.timothyc.sparklet.Partition;
import info.timothyc.sparklet.expr.Row;

import java.util.List;
import java.util.function.Function;

class Messages {
    private Messages() {}

    private static class BaseMessage {
        final int id;

        private BaseMessage(int id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + ":" + id;
        }
    }

    static class Response extends BaseMessage {
        final Object data;

        Response(int id, Object data) {
            super(id);
            this.data = data;
        }
    }

    public static class Set extends BaseMessage {
        final List<List<?>> data;

        public Set(int id, List<List<?>> data) {
            super(id);
            this.data = data;
        }
    }

    static class Append extends BaseMessage {
        final List<?> row;

        Append(int id, List<?> row) {
            super(id);
            this.row = row;
        }
    }

    static class Repartition extends BaseMessage {
        final int columnIndex;
        final List<Handle> handles;

        Repartition(int id, int columnIndex, List<Handle> handles) {
            super(id);
            this.columnIndex = columnIndex;
            this.handles = handles;
        }
    }

    static class Map extends BaseMessage {
        final Function<Row, Row> transform;
        final Handle destination;

        Map(Integer id, Function<Row, Row> transform, Handle destination) {
            super(id);
            this.transform = transform;
            this.destination = destination;
        }
    }

    static class Count extends BaseMessage {
        Count(int id) {
            super(id);
        }
    }

    static class Retrieve extends BaseMessage {
        Retrieve(int id) {
            super(id);
        }
    }
}
