package info.timothyc.sparklet;

import com.sun.istack.internal.Nullable;
import info.timothyc.sparklet.expr.Row;
import scala.Unit;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Partition {

    private final List<Integer> indices;
    private final Handle handle;

    public Partition(@Nullable List<Integer> indices, Handle handle) {
        this.indices = indices != null ? indices : Collections.emptyList();
        this.handle = handle;
    }

    public CompletableFuture<Void> repartition(int columnIndex, List<Partition> newPartitions) {
        List<Handle> handles = newPartitions.stream()
                .map(p -> p.handle)
                .collect(Collectors.toList());
        return handle.repartition(columnIndex, handles);
    }

    public CompletableFuture<Integer> count() {
        return handle.count();
    }

    public CompletableFuture<Iterable<?>> retrieve() {
        return handle.retrieve();
    }

    public CompletableFuture<Void> map(Function<Row, Row> transform, Partition newPartition) {
        return handle.map(transform, newPartition.handle);
    }
}
