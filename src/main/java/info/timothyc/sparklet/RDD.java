package info.timothyc.sparklet;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Futures;
import info.timothyc.sparklet.expr.Row;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RDD {

    private final HandleFactory hf;
    private final List<Partition> partitions;

    public RDD(HandleFactory hf, List<Partition> partitions) {
        this.hf = hf;
        this.partitions = partitions;
    }

    public static <L extends Iterable<E>, E extends Iterable<?>> RDD from(HandleFactory hf, L things) {
       Partition p = new Partition(null, hf.from(things));
       return new RDD(hf, Lists.newArrayList(p));
    }

    public CompletableFuture<RDD> repartition(int numPartitions, int columnIndex) {
        List<Partition> newPartitions = Lists.newArrayListWithExpectedSize(numPartitions);
        for (int i = 0; i < numPartitions; ++i) {
            newPartitions.add(new Partition(Lists.newArrayList(columnIndex), hf.empty()));
        }

        CompletableFuture<Void>[] tasks = new CompletableFuture[partitions.size()];
        for (int i = 0; i < partitions.size(); ++i) {
            tasks[i] = partitions.get(i).repartition(columnIndex, newPartitions);
        }

        return CompletableFuture.allOf(tasks).thenApply(_void -> new RDD(hf, newPartitions));
    }

    public CompletableFuture<Integer> count() {
        AtomicInteger count = new AtomicInteger();
        CompletableFuture<Integer>[] counts = new CompletableFuture[partitions.size()];
        for (int i = 0; i < partitions.size(); ++i) {
            counts[i] = partitions.get(i).count().thenApply(count::addAndGet);
        }

        return CompletableFuture.allOf(counts).thenApply(_void -> count.get());
    }

    public CompletableFuture<RDD> map(Function<Row, Row> transform) {
        List<Partition> newPartitions = Lists.newArrayListWithExpectedSize(partitions.size());
        for (int i = 0; i < partitions.size(); ++i) {
            newPartitions.add(new Partition(null, hf.empty()));
        }
        CompletableFuture<Void>[] tasks = new CompletableFuture[partitions.size()];
        for (int i = 0; i < partitions.size(); ++i) {
            tasks[i] = partitions.get(i).map(transform, newPartitions.get(i));
        }

        return CompletableFuture.allOf(tasks).thenApply(_void -> new RDD(hf, newPartitions));
    }

    public Future<Iterable<?>> retrieve() {
        Collection<Iterable<?>> its = new ConcurrentLinkedDeque<>();
        CompletableFuture<?>[] bits = new CompletableFuture[partitions.size()];
        for (int i = 0; i < partitions.size(); ++i) {
            bits[i] = partitions.get(i).retrieve().thenApply(its::add);
        }

        return CompletableFuture.allOf(bits).thenApply(_void -> Iterables.concat(its));
    }
}
