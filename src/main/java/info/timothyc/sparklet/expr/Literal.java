package info.timothyc.sparklet.expr;

import java.util.List;

public class Literal implements Expression {
    private final Object value;

    Literal(Object value) {
        this.value = value;
    }

    @Override
    public Object evaluate(List<?> row) {
        return this.value;
    }
}
