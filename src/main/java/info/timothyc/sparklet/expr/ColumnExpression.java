package info.timothyc.sparklet.expr;

import java.util.List;

public class ColumnExpression implements Expression {
    private final int column;

    public ColumnExpression(int column) {
        this.column = column;
    }

    @Override
    public Object evaluate(List<?> row) {
        return row.get(column);
    }
}
