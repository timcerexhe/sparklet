package info.timothyc.sparklet.expr;

import java.util.List;
import java.util.Objects;

public class PlusExpression implements Expression {
    private final Expression left;
    private final Expression right;

    PlusExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Object evaluate(List<?> row) {
        Object l = left.evaluate(row);
        Object r = right.evaluate(row);

        // TODO support other conversion pairs
        assert l instanceof Integer && r instanceof Integer;
        return ((Integer) l) + ((Integer) r);
    }
}
