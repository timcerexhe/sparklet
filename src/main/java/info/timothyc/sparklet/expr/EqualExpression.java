package info.timothyc.sparklet.expr;

import java.util.List;
import java.util.Objects;

public class EqualExpression implements Expression {
    private final Expression left;
    private final Expression right;

    public EqualExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Object evaluate(List<?> row) {
        return Objects.equals(left.evaluate(row), right.evaluate(row));
    }
}
