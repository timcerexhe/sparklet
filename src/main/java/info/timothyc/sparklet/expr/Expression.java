package info.timothyc.sparklet.expr;

import java.util.List;

public interface Expression {
    Object evaluate(List<?> row);
}
