package info.timothyc.sparklet.expr;

import com.google.common.collect.Lists;

import java.util.List;

public class Row {

    public static Expression eq(Expression left, Expression right) {
        return new EqualExpression(left, right);
    }

    public static Expression plus(Expression left, Expression right) {
        return new PlusExpression(left, right);
    }

    public static Expression mul(Expression left, Expression right) {
        return new MultiplyExpression(left, right);
    }

    public static Literal lit(int num) {
        return new Literal(num);
    }

    public static Expression c(int column) {
        return new ColumnExpression(column);
    }

    public final List<?> data;

    public Row(List<?> data) {
        this.data = data;
    }

    public Row drop(int column) {
        List<Object> copy = Lists.newArrayListWithExpectedSize(data.size());
        for (int i = 0; i < data.size(); ++i) {
            if (i != column) {
                copy.add(data.get(i));
            }
        }
        return new Row(copy);
    }

    public Row add(Expression e) {
        List<Object> copy = Lists.newArrayList(this.data);
        copy.add(e.evaluate(data));
        return new Row(copy);
    }
}
