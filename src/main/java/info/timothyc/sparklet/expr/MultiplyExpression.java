package info.timothyc.sparklet.expr;

import java.util.List;

public class MultiplyExpression implements Expression {
    private final Expression left;
    private final Expression right;

    MultiplyExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Object evaluate(List<?> row) {
        Object l = left.evaluate(row);
        Object r = right.evaluate(row);

        // TODO support other conversion pairs
        assert l instanceof Integer && r instanceof Integer;
        return ((Integer) l) * ((Integer) r);
    }
}
