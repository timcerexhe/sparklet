package info.timothyc.sparklet;

public interface HandleFactory {

    <L extends Iterable<E>, E extends Iterable<?>> Handle empty();
    <L extends Iterable<E>, E extends Iterable<?>> Handle from(L elements);
}
