package info.timothyc.sparklet;

import info.timothyc.sparklet.expr.Row;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

public interface Handle {
    CompletableFuture<Void> repartition(int columnIndex, List<Handle> newPartitions);
    CompletableFuture<Void> map(Function<Row, Row> transform, Handle newPartition);
    CompletableFuture<Void> append(List<?> row);
    CompletableFuture<Integer> count();
    CompletableFuture<Iterable<?>> retrieve();
}
